import React, { PureComponent } from "react";
import { Icon } from "semantic-ui-react";
import "./home.css";
const TableContents = props => {
	return (
		<div className="table_row">
			<div>
				<Icon name={props.iconName} size="large" color="blue" />
				{props.text}
			</div>
			<div>
				<Icon
					disabled={props.minusDisable}
					name="minus circle"
					size="large"
					onClick={e =>
						props.handleOnClick(e, props.text, "decrease")
					}
					style={{ cursor: "pointer" }}
				/>
			</div>
			<div style={{ color: "gray" }}>{props.value}</div>
			<div>
				<Icon
					disabled={props.plusDisable}
					name="plus circle"
					size="large"
					color="red"
					onClick={e =>
						props.handleOnClick(e, props.text, "increase")
					}
					style={{ cursor: "pointer" }}
				/>
			</div>
		</div>
	);
};
export default class Home extends PureComponent {
	state = {
		roomCount: 1,
		adultCount: 1,
		childCount: 0,
		personCount: 1,
		roomDecreseButtonDisabled: true,
		roomIncreaseButtonDisable: false,
		adultDecreseButtonDisabled: true,
		adultIncreaseButtonDisable: false,
		childDecreseButtonDisabled: true,
		childIncreaseButtonDisable: false,
		maxNum: 4
	};
	handleOnClick = (e, name, type) => {
		if (type === "increase") {
			if (name === "Room") {
				if (this.state.roomCount < 5) {
					this.setState({
						roomCount: this.state.roomCount + 1,
						roomDecreseButtonDisabled: false,
						adultCount: this.state.adultCount + 1,
						personCount: this.state.personCount + 1,
						maxNum: this.state.maxNum + 4
					});
					if (this.state.roomCount === 4) {
						this.setState({
							roomIncreaseButtonDisable: true
						});
					}
					if (this.state.adultCount >= 1)
						this.setState({
							adultDecreseButtonDisabled: false
						});
				}
			}
			if (name === "Adult") {
				if (this.state.personCount < this.state.maxNum) {
					this.setState({
						personCount: this.state.personCount + 1,
						adultCount: this.state.adultCount + 1,
						adultDecreseButtonDisabled: false
					});
				} else {
					if (this.state.roomCount < 5) {
						this.setState(
							{
								roomCount: this.state.roomCount + 1,
								maxNum: this.state.maxNum + 4,
								adultCount: this.state.adultCount + 1,
								personCount: this.state.personCount + 1
							},
							() => {
								if (this.state.personCount >= 5) {
									this.setState({
										roomDecreseButtonDisabled: false
									});
								}
								if (this.state.roomCount === 5) {
									this.setState({
										roomIncreaseButtonDisable: true
									});
								}
							}
						);
					} else
						this.setState({
							adultIncreaseButtonDisable: true,
							childIncreaseButtonDisable: true
						});
				}
			}
			if (name === "Child") {
				if (this.state.personCount < this.state.maxNum) {
					this.setState({
						personCount: this.state.personCount + 1,
						childCount: this.state.childCount + 1,
						childDecreseButtonDisabled: false
					});
				} else {
					if (this.state.roomCount < 5) {
						this.setState(
							{
								roomCount: this.state.roomCount + 1,
								maxNum: this.state.maxNum + 4,
								childCount: this.state.childCount + 1,
								personCount: this.state.personCount + 2,
								adultCount: this.state.adultCount + 1
							},
							() => {
								if (this.state.personCount >= 5) {
									this.setState({
										roomDecreseButtonDisabled: false
									});
								}
								if (this.state.roomCount === 5) {
									this.setState({
										roomIncreaseButtonDisable: true
									});
								}
							}
						);
					}
					if (this.state.personCount >= 20)
						this.setState({
							childIncreaseButtonDisable: true,
							adultIncreaseButtonDisable: true
						});
				}
			}
		}

		if (type === "decrease") {
			if (name === "Room") {
				this.setState(
					{
						roomCount: this.state.roomCount - 1,
						roomIncreaseButtonDisable: false,
						maxNum: this.state.maxNum - 4
					},
					() => {
						if (
							this.state.roomCount === 4 ||
							this.state.roomCount === 2 ||
							this.state.roomCount === 3
						) {
							if (this.state.childCount >= 4) {
								this.setState({
									personCount: this.state.personCount - 4,
									childCount: this.state.childCount - 4,
									childIncreaseButtonDisable: false,
									adultIncreaseButtonDisable: false
								});
							} else {
								let reminder = 4 - this.state.childCount;
								this.setState({
									personCount: this.state.personCount - 4,
									childCount: 0,
									childIncreaseButtonDisable: false,
									adultCount:
										this.state.adultCount - reminder,
									childDecreseButtonDisabled: true,
									adultIncreaseButtonDisable: false
								});
							}
						}
						if (this.state.roomCount === 1) {
							if (this.state.adultCount >= 4) {
								this.setState({
									personCount: this.state.personCount - 4,

									childCount: 0,
									childDecreseButtonDisabled: true,
									adultCount: 4,
									adultIncreaseButtonDisable: false,
									childIncreaseButtonDisable: false
								});
							} else {
								this.setState({
									childDecreseButtonDisabled: true,
									personCount:
										this.state.personCount -
										this.state.childCount,
									childIncreaseButtonDisable: false,
									childCount: 0,
									adultIncreaseButtonDisable: false
								});
							}
						}
					}
				);
				if (this.state.roomCount <= 2) {
					this.setState({
						roomDecreseButtonDisabled: true
					});
				}
			} else if (name === "Adult") {
				this.setState(
					{
						personCount: this.state.personCount - 1,
						adultCount: this.state.adultCount - 1,
						adultIncreaseButtonDisable: false
					},
					() => {
						if (this.state.personCount === 16) {
							this.setState({
								roomCount: 4,
								maxNum: 16,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount === 12) {
							this.setState({
								roomCount: 3,
								maxNum: 12,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount === 8) {
							this.setState({
								roomCount: 2,
								maxNum: 8,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount <= 4) {
							this.setState({
								roomCount: 1,
								maxNum: 4,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.adultCount === 1) {
							this.setState({
								adultDecreseButtonDisabled: true
							});
						}
						if (this.state.personCount <= 4) {
							this.setState({
								roomDecreseButtonDisabled: true
							});
						}
					}
				);
			} else if (name === "Child") {
				this.setState(
					{
						personCount: this.state.personCount - 1,
						childCount: this.state.childCount - 1,
						adultIncreaseButtonDisable: false,
						childIncreaseButtonDisable: false
					},
					() => {
						if (this.state.personCount === 16) {
							this.setState({
								roomCount: 4,
								maxNum: 16,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount === 12) {
							this.setState({
								roomCount: 3,
								maxNum: 12,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount === 8) {
							this.setState({
								roomCount: 2,
								maxNum: 8,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount <= 4) {
							this.setState({
								roomCount: 1,
								maxNum: 4,
								roomIncreaseButtonDisable: false
							});
						}
						if (this.state.childCount === 0) {
							this.setState({
								childDecreseButtonDisabled: true,
								childIncreaseButtonDisable: false
							});
						}
						if (this.state.personCount <= 4) {
							this.setState({
								roomDecreseButtonDisabled: true
							});
						}
					}
				);
			}
		}
	};
	render() {
		// console.log("room", this.state.roomCount);
		// console.log("adult", this.state.adultCount);
		// console.log("persons", this.state.personCount);
		// console.log("maxNo", this.state.maxNum);
		let {
			roomCount,
			adultCount,
			childCount,
			roomDecreseButtonDisabled,
			roomIncreaseButtonDisable,
			adultDecreseButtonDisabled,
			adultIncreaseButtonDisable,
			childDecreseButtonDisabled,
			childIncreaseButtonDisable
		} = this.state;
		return (
			<React.Fragment>
				<div className="home_container">
					<div>
						<Icon name="users" size="large" /> Choose number of{" "}
						<b>people</b>
					</div>
					<div className="table_container">
						<TableContents
							iconName="bed"
							text="Room"
							handleOnClick={this.handleOnClick}
							value={roomCount}
							minusDisable={roomDecreseButtonDisabled}
							plusDisable={roomIncreaseButtonDisable}
						/>
						<TableContents
							iconName="user"
							text="Adult"
							handleOnClick={this.handleOnClick}
							value={adultCount}
							minusDisable={adultDecreseButtonDisabled}
							plusDisable={adultIncreaseButtonDisable}
						/>
						<TableContents
							iconName="child"
							text="Child"
							handleOnClick={this.handleOnClick}
							value={childCount}
							minusDisable={childDecreseButtonDisabled}
							plusDisable={childIncreaseButtonDisable}
						/>
					</div>
				</div>
			</React.Fragment>
		);
	}
}
